// ViconSDK_PVA.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "comms.h"
#include "PVA.h"


#define PI	3.1415926;



PVA_DATA pva ;


#ifdef WIN32
  bool Hit()
  {
    bool hit = false;
    while( _kbhit() )
    {
      getchar();
      hit = true;
    }
    return hit;                                                                          
  }
#endif

void init_pva(void)
{
	pva.accel[0]=0.0f ;		pva.accel[1]=0.0f ; 	pva.accel[2] = 0.0f ;
	pva.pos[0] = 0.0f ;		pva.pos[1] = 0.0f ;		pva.pos[2] = 0.0f ;
	pva.rates[0] = 0.0f;	pva.rates[1] = 0.0f;	pva.rates[2] = 0.0f ;
	pva.vel[0] = 0.0f ;		pva.vel[1] = 0.0f ;		pva.vel[2] = 0.0f ;
	pva.angles[0] = 0.0f ;	pva.angles[1]=0.0f ;	pva.angles[2] = 0.0f ;
}


int main( int argc, char* argv[] )
{
  std::string HostName = "localhost:801";
  
  init_pva() ;
  init_socket() ;

  std::ofstream ofs;

  // Make a new client
  Client MyClient;

  for(int i=0; i != 3; ++i) // repeat to check disconnecting doesn't wreck next connect
  {
    // Connect to a server
    std::cout << "Connecting to " << HostName << " ..." << std::flush;
    while( !MyClient.IsConnected().Connected )
    {
      // Direct connection

      bool ok = false;
      ok =( MyClient.Connect( HostName ).Result == Result::Success );
     
      if(!ok)
      {
        std::cout << "Warning - connect failed..." << std::endl;
      }


      std::cout << ".";
  #ifdef WIN32
      Sleep( 200 );
  #else
      sleep(1);
  #endif
    }
    std::cout << std::endl;

    // Enable some different data types
    MyClient.EnableSegmentData();
    MyClient.EnableMarkerData();
    MyClient.EnableUnlabeledMarkerData();
    MyClient.EnableDeviceData();

    std::cout << "Segment Data Enabled: "          << Adapt( MyClient.IsSegmentDataEnabled().Enabled )         << std::endl;
    std::cout << "Marker Data Enabled: "           << Adapt( MyClient.IsMarkerDataEnabled().Enabled )          << std::endl;
    std::cout << "Unlabeled Marker Data Enabled: " << Adapt( MyClient.IsUnlabeledMarkerDataEnabled().Enabled ) << std::endl;
    std::cout << "Device Data Enabled: "           << Adapt( MyClient.IsDeviceDataEnabled().Enabled )          << std::endl;
    // Set the streaming mode
    //MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPull );
    // MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPullPreFetch );
    MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ServerPush );

    // Set the global up axis
    //MyClient.SetAxisMapping( Direction::Forward, 
    //                         Direction::Left, 
    //                         Direction::Up ); // Z-up (VICON default)
    MyClient.SetAxisMapping( Direction::Forward, 
                               Direction::Right, 
                               Direction::Down ); // Standard Aircraft

    Output_GetAxisMapping _Output_GetAxisMapping = MyClient.GetAxisMapping();
    std::cout << "Axis Mapping: X-" << Adapt( _Output_GetAxisMapping.XAxis ) 
                           << " Y-" << Adapt( _Output_GetAxisMapping.YAxis ) 
                           << " Z-" << Adapt( _Output_GetAxisMapping.ZAxis ) << std::endl;

    size_t FrameRateWindow = 1000; // frames
    size_t Counter = 0;
    clock_t LastTime = clock();
    // Loop until a key is pressed
  #ifdef WIN32
    while( !Hit())
	
  #else
    while( true)
  #endif
    {
      // Get a frame
     // output_stream << "Waiting for new frame...";
      while( MyClient.GetFrame().Result != Result::Success )
      {
        // Sleep a little so that we don't lumber the CPU with a busy poll
        #ifdef WIN32
          Sleep( 200 );
        #else
          sleep(1);
        #endif

        //output_stream << ".";
      }
      //output_stream << std::endl;
	  
      // Get the frame number
      //Output_GetFrameNumber FrameNumber = MyClient.GetFrameNumber();
	  pva.frame_number = MyClient.GetFrameNumber().FrameNumber;

      // Count the number of subjects
      unsigned int SubjectCount = MyClient.GetSubjectCount().SubjectCount;
      //output_stream << "Subjects (" << SubjectCount << "):" << std::endl;
	  
//-------------------------------------------------------------------------------------------------------------------------------

      for( unsigned int SubjectIndex = 0 ; SubjectIndex < SubjectCount ; ++SubjectIndex )
      {
        //output_stream << "  Subject #" << SubjectIndex << std::endl;

        // Get the subject name
        std::string SubjectName = MyClient.GetSubjectName( SubjectIndex ).SubjectName;
        //output_stream << "    Name: " << SubjectName << std::endl;

        // Get the root segment
        std::string RootSegment = MyClient.GetSubjectRootSegmentName( SubjectName ).SegmentName;
        //output_stream << "    Root Segment: " << RootSegment << std::endl;

        // Count the number of segments
        unsigned int SegmentCount = MyClient.GetSegmentCount( SubjectName ).SegmentCount;
       // output_stream << "    Segments (" << SegmentCount << "):" << std::endl;

		//Output_GetSegmentGlobalTranslation GlobalTranslation = 
		   //MyClient.GetSegmentGlobalTranslation("pelicanlp2", "pelicanlp2" );		// Changed to Pelican

		if(SubjectName == "ardrone2"){

		Output_GetSegmentGlobalTranslation GlobalTranslation = 
			MyClient.GetSegmentGlobalTranslation("ardrone2", "ardrone2" );		

        //    MyClient.GetSegmentGlobalTranslation( SubjectName, RootSegment );

		 float temp ;
         pva.pos[0] = (float) GlobalTranslation.Translation[ 0 ] * 0.001 ;			pva.pos[0]=-pva.pos[0];	     // flip x axis
         pva.pos[1] = (float) GlobalTranslation.Translation[ 1 ] * 0.001 ;			pva.pos[1]=-pva.pos[1];	     // flip y axis
         pva.pos[2] = (float) GlobalTranslation.Translation[ 2 ] * 0.001 ;			//pva.pos[2]-=-0.10;				 // Calibrate z axis
         pva.pos[2]=-pva.pos[2];
		 
		 Output_GetSegmentGlobalRotationEulerXYZ GlobalEulerXYZ = 
           MyClient.GetSegmentGlobalRotationEulerXYZ( "ardrone2", "ardrone2" );		

		// Output_GetSegmentGlobalRotationEulerXYZ GlobalEulerXYZ = 
          // MyClient.GetSegmentGlobalRotationEulerXYZ( "pelicanlp2", "pelicanlp2" );												// Changed to Pelican

		// angles in radians
		 pva.angles[0] = (float) GlobalEulerXYZ.Rotation[ 0 ] ;
		 pva.angles[1] = (float) GlobalEulerXYZ.Rotation[ 1 ] ;
		 pva.angles[2] = (float) GlobalEulerXYZ.Rotation[ 2 ] ;
		 //pva.angles[2] -= (-1.586) ;
																													// Yaw angle "pva.angles[2]" as Pelican yaw angle
		 if(pva.angles[2]<0)																						// Yaw range from 0~PI, then -PI~0, so plus 2PI
		 {	pva.angles[2]+=2*PI;	}

//		 pva.angles[2]=180*pva.angles[2]/PI;
		 
//		 if(pva.angles[2]>=90 && pva.angles[2]<=360)																// re-define 0 degree of yaw
//		 {	pva.angles[2]-=90;	}
//		 else
//		 {	pva.angles[2]+=270;	}		

	
		 std::cout << "X=" << pva.pos[0] << ", Y=" << pva.pos[1] << ", Z=" << pva.pos[2] << std::endl ;
		 std::cout << "angle yaw=" << pva.angles[2]<<std::endl;

		 send_PVA_data() ;
		}

		if ( SubjectName == "xlkasd:"){


		}
      }
	  
    }

    MyClient.DisableSegmentData();
    MyClient.DisableMarkerData();
    MyClient.DisableUnlabeledMarkerData();
    MyClient.DisableDeviceData();

    // Disconnect and dispose
    int t = clock();
    std::cout << " Disconnecting..." << std::endl;
    MyClient.Disconnect();
    int dt = clock() - t;
    double secs = (double) (dt)/(double)CLOCKS_PER_SEC;
    std::cout << " Disconnect time = " << secs << " secs" << std::endl;

  }

  close_socket() ;
}
