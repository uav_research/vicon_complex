Don't forget to
- connect to the client (e.g. drone or PC104 router)
- change the IP address to match the client (e.g. 192.168.1.1 for drone or 192.168.1.52 for PC104)
