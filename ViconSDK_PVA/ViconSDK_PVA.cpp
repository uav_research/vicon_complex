// ViconSDK_PVA.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "comms.h"
#include "PVA.h"

#include "pugixml.hpp"

#define PI	3.1415926
using namespace std;
PVA_DATA pva ;


#ifdef WIN32
  bool Hit()
  {
    bool hit = false;
    while( _kbhit() )
    {
      getchar();
      hit = true;
    }
    return hit;                                                                          
  }
#endif
 int import_drones(void);
  //*--------------------------------------------------------------------------------------------------------------------------------*//
  int number_of_drones = 0;
  //*--------------------------------------------------------------------------------------------------------------------------------*//
	void init_pva(void)
	{
	import_drones();

	for(int index=0; index < number_of_drones; index++ )
   {
	   pva.drones[index].position[0] = 0.0f;
	   pva.drones[index].position[1] = 0.0f;
	   pva.drones[index].position[2] = 0.0f;
	   pva.drones[index].orientation[0] = 0.0f;
	   pva.drones[index].orientation[1] = 0.0f;
	   pva.drones[index].orientation[2] = 0.0f;
   }
}
//*--------------------------------------------------------------------------------------------------------------------------------*//
//*--------------------------------------------------------------------------------------------------------------------------------*//

	int import_drones(void) {
		
		 pugi::xml_document doc;
		if (!doc.load_file("drones.xml")) return -1;
		pugi::xml_node tools = doc.child("boss");

    // tag::basic[]
    for (pugi::xml_node tool = tools.first_child(); tool; tool = tool.next_sibling())
    {
		number_of_drones++;
    }
	pva.drones = new Drones[number_of_drones];
	int index = 0;
	for (pugi::xml_node tool = tools.first_child(); tool; tool = tool.next_sibling())
	{
		pva.drones[index].name = tool.attribute("name").value();
		pva.drones[index].IP = tool.attribute("IP").value();
		index++;
    }   
	return 0;
	}

int main( int argc, char* argv[] )
{
  std::string HostName = "localhost:801";
  
  init_pva() ;
  init_socket() ;
 

  int updatefrequency = 0 ;

  std::ofstream ofs;

  // Make a new client
  Client MyClient;

  for(int i=0; i != 3; ++i) // repeat to check disconnecting doesn't wreck next connect
  {
    // Connect to a server
    std::cout << "Connecting to " << HostName << " ..." << std::flush;
    while( !MyClient.IsConnected().Connected )
    {
      // Direct connection

      bool ok = false;
      ok =( MyClient.Connect( HostName ).Result == Result::Success );
     
      if(!ok)
      {
        std::cout << "Warning - connect failed..." << std::endl;
      }


      std::cout << ".";
  #ifdef WIN32
      Sleep(200);
  #else
      sleep(1);
  #endif
    }
    std::cout << std::endl;

    // Enable some different data types
    MyClient.EnableSegmentData();
    MyClient.EnableMarkerData();
    MyClient.EnableUnlabeledMarkerData();
    MyClient.EnableDeviceData();

    std::cout << "Segment Data Enabled: "          << Adapt( MyClient.IsSegmentDataEnabled().Enabled )         << std::endl;
    std::cout << "Marker Data Enabled: "           << Adapt( MyClient.IsMarkerDataEnabled().Enabled )          << std::endl;
    std::cout << "Unlabeled Marker Data Enabled: " << Adapt( MyClient.IsUnlabeledMarkerDataEnabled().Enabled ) << std::endl;
    std::cout << "Device Data Enabled: "           << Adapt( MyClient.IsDeviceDataEnabled().Enabled )          << std::endl;
    // Set the streaming mode
    //MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPull );
    // MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPullPreFetch );
    MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ServerPush );

    // Set the global up axis
    //MyClient.SetAxisMapping( Direction::Forward, 
    //                         Direction::Left, 
    //                         Direction::Up ); // Z-up (VICON default)

    MyClient.SetAxisMapping( Direction::Forward, 
                               Direction::Right, 
                               Direction::Down ); // NED !!!IMPORTANT MAVROS USES ENU SO TRANSLATE AS APPROPRIATE ON THE OTHER END

    Output_GetAxisMapping _Output_GetAxisMapping = MyClient.GetAxisMapping();
    std::cout << "Axis Mapping: X-" << Adapt( _Output_GetAxisMapping.XAxis ) 
                           << " Y-" << Adapt( _Output_GetAxisMapping.YAxis ) 
                           << " Z-" << Adapt( _Output_GetAxisMapping.ZAxis ) << std::endl;

    size_t FrameRateWindow = 1000; // frames
    size_t Counter = 0;
    clock_t LastTime = clock();
    // Loop until a key is pressed
  #ifdef WIN32
    while( !Hit())
	
  #else
    while( true)
  #endif
    {
      // Get a frame
     // output_stream << "Waiting for new frame...";
      while( MyClient.GetFrame().Result != Result::Success )
      {
        // Sleep a little so that we don't lumber the CPU with a busy poll
        #ifdef WIN32
          Sleep( 200 );
        #else
          sleep(1);
        #endif

        //output_stream << ".";
      }
      //output_stream << std::endl;
	  
      // Get the frame number
      //Output_GetFrameNumber FrameNumber = MyClient.GetFrameNumber();
	  pva.frame_number = MyClient.GetFrameNumber().FrameNumber;

      // Count the number of subjects
      unsigned int SubjectCount = MyClient.GetSubjectCount().SubjectCount;
      //output_stream << "Subjects (" << SubjectCount << "):" << std::endl;
	  
		//-------------------------------------------------------------------------------------------------------------------------------

      for( unsigned int SubjectIndex = 0 ; SubjectIndex < SubjectCount ; ++SubjectIndex )
      {
        //output_stream << "  Subject #" << SubjectIndex << std::endl;

        // Get the subject name
        std::string SubjectName = MyClient.GetSubjectName( SubjectIndex ).SubjectName;
        //output_stream << "    Name: " << SubjectName << std::endl;

        // Get the root segment
        std::string RootSegment = MyClient.GetSubjectRootSegmentName( SubjectName ).SegmentName;
        //output_stream << "    Root Segment: " << RootSegment << std::endl;

        // Count the number of segments
        unsigned int SegmentCount = MyClient.GetSegmentCount( SubjectName ).SegmentCount;
       // output_stream << "    Segments (" << SegmentCount << "):" << std::endl;

		//*--------------------------------------------------------------------------------------------------------------------------------*//
		//*--------------------------------------------------------------------------------------------------------------------------------*//
		
		for(int index=0; index < number_of_drones; index++ )
		{
			if( SubjectName == pva.drones[index].name){

		  Output_GetSegmentGlobalTranslation GlobalTranslation = 
			  MyClient.GetSegmentGlobalTranslation(pva.drones[index].name,pva.drones[index].name);

         pva.drones[index].position[0] = (float) GlobalTranslation.Translation[ 0 ] * 0.001 ;			pva.drones[index].position[0]=-pva.drones[index].position[0];	     // flip x axis
         pva.drones[index].position[1] = (float) GlobalTranslation.Translation[ 1 ] * 0.001 ;			pva.drones[index].position[1]=-pva.drones[index].position[1];	     // flip y axis
         pva.drones[index].position[2] = (float) GlobalTranslation.Translation[ 2 ] * 0.001 ;			pva.drones[index].position[2]=+pva.drones[index].position[2]+0.159;				 // Calibrate z axis

		 //Debug line to check you are getting the correct values
		  cout<<pva.drones[index].name<<" :  " <<pva.drones[index].position[0]<<" "<<pva.drones[index].position[1]<<" "<<pva.drones[index].position[2]<<endl;

		 
			 Output_GetSegmentGlobalRotationEulerXYZ GlobalEulerXYZ = 
           MyClient.GetSegmentGlobalRotationEulerXYZ(pva.drones[index].name,pva.drones[index].name);		

			// angles in radians
			pva.drones[index].orientation[0] = (float) GlobalEulerXYZ.Rotation[ 0 ] ;
			pva.drones[index].orientation[1] = (float) GlobalEulerXYZ.Rotation[ 1 ] ;
			 pva.drones[index].orientation[2] = (float) GlobalEulerXYZ.Rotation[ 2 ] ;
																													
			 if(pva.drones[index].orientation[2]<0)																						// Yaw range from 0~PI, then -PI~0, so plus 2PI
				{	pva.drones[index].orientation[2]+=2*PI;	}
			}//END IF//
		 }
	  }	
	  send_PVA_data() ; 
    }
	//*--------------------------------------------------------------------------------------------------------------------------------*//

	//*--------------------------------------------------------------------------------------------------------------------------------*//

    MyClient.DisableSegmentData();
    MyClient.DisableMarkerData();
    MyClient.DisableUnlabeledMarkerData();
    MyClient.DisableDeviceData();

    // Disconnect and dispose
    int t = clock();
    std::cout << " Disconnecting..." << std::endl;
    MyClient.Disconnect();
    int dt = clock() - t;
    double secs = (double) (dt)/(double)CLOCKS_PER_SEC;
    std::cout << " Disconnect time = " << secs << " secs" << std::endl;

  }
		 close_socket() ;
}
