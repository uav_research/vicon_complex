

#ifndef CommPortClass
#define CommPortClass

class CommPort
{
private:
	HANDLE hPort ;
public:
	CommPort() { hPort = NULL ; }
	~CommPort() { }
	bool OpenPort(CString PortSpecifier) ;
	int WritePort(CString data, DWORD num_bytes) ;
	void ClosePort() ;
} ;


bool CommPort::OpenPort(CString PortSpecifier)
{
	DCB dcb ;

	hPort = CreateFile(
		PortSpecifier,
		GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		0,
		NULL
	);

	if (!GetCommState(hPort,&dcb))
		return false ;

	dcb.BaudRate = CBR_9600 ;
	dcb.ByteSize = 8 ;
	dcb.Parity = NOPARITY ;
	dcb.StopBits = ONESTOPBIT ;

	if (!SetCommState(hPort, &dcb))
		return false ;

	return true ;
}

void CommPort::ClosePort()
{
	CloseHandle(hPort) ;
}


int CommPort::WritePort(CString data, DWORD num_bytes)
{
	DWORD byteswritten ;

	WriteFile(hPort, data, num_bytes, &byteswritten, NULL) ;

	return byteswritten;
}



#endif
